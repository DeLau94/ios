//
//  HighscoresController.swift
//  Simon Game
//
//  Created by Laurens Peremans on 06/12/15.
//  Copyright © 2015 Laurens Peremans. All rights reserved.
//

import Foundation
import UIKit
import CoreData
class HighscoreController:UITableViewController{
    var i=0
    var users:[user] = []
    var headerPresent = false
    override func viewDidLoad() {
        
        
        // core data
        let app:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        let context:NSManagedObjectContext = app.managedObjectContext
        let request = NSFetchRequest(entityName: "User")
        
        do {
            let results = try context.executeFetchRequest(request) as! [user]
            users = results as [user]
            users.sortInPlace({ $0.score > $1.score })
        } catch {
            print("Something went wrong!")
        }

        
    }
    
    @IBAction func Menu(sender: AnyObject) {
        performSegueWithIdentifier("highscores", sender: self)
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return users.count
    }
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier( "LabelCell", forIndexPath: indexPath) as! HighscoresView
        
        if(users.count>i){
            cell.nameLbl.text = users[i].name
            cell.scoreLbl.text = String(users[i].score)
            i+=1
        }
       
        
        return cell
    }
    
    
    
}