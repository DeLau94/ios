//
//  ColorModel.swift
//  Simon Game
//
//  Created by Laurens Peremans on 03/12/15.
//  Copyright © 2015 Laurens Peremans. All rights reserved.
//

import UIKit

struct Colour
{
    let name: String
    let colour: UIColor
    
  }
class ColorModel{
    var colors:[Colour]=[]
    
    let blue = Colour(name: "Blue", colour:UIColor.blueColor())
    let red = Colour(name: "Red", colour:UIColor.redColor())
    let green = Colour(name: "Green", colour:UIColor.greenColor())
    let yellow = Colour(name: "Yellow", colour:UIColor.yellowColor())
    
    let lightBlue = Colour(name:"LightBlue", colour:UIColor(red:0.37, green:0.62, blue:1.00, alpha:1.0))
    let lightRed = Colour(name:"LightRed", colour:UIColor(red:1.00, green:0.32, blue:0.32, alpha:1.0))
    let lightGreen = Colour(name:"LightGreen", colour:UIColor(red:0.64, green:1.00, blue:0.50, alpha:1.0))
    let lightYellow = Colour(name:"LightYellow", colour:UIColor(red:1.00, green:1.00, blue:0.66, alpha:1.0))
    
    
    init(){
    colors.append(blue)
    colors.append(red)
    colors.append(green)
    colors.append(yellow)

    }
    func getColor(naam:String)->UIColor{
        for item in colors{
            if(item.name==naam){return item.colour}
            
        }
  return UIColor.blueColor()
    }
    
}