//
//  TurnModel.swift
//  Simon Game
//
//  Created by Laurens Peremans on 04/12/15.
//  Copyright © 2015 Laurens Peremans. All rights reserved.
//

import Foundation

class TurnModel{
    let computer = "Computer turn!"
    let user = "User turn!"
    let gameOver = "Game over!"
    var count = 0 //if all colors are shown the turn changes to the user or the computer
    var score = 0

}