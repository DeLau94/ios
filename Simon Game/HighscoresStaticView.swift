//
//  HighscoresStaticView.swift
//  Simon Game
//
//  Created by Laurens Peremans on 18/12/15.
//  Copyright © 2015 Laurens Peremans. All rights reserved.
//


import UIKit
class HighscoresStaticView: UITableViewCell{

    @IBOutlet weak var staticName: UILabel!
    @IBOutlet weak var staticScore: UILabel!
}