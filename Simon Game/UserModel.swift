//
//  UserModel.swift
//  Simon Game
//
//  Created by Laurens Peremans on 07/12/15.
//  Copyright © 2015 Laurens Peremans. All rights reserved.
//

import Foundation
import CoreData

class user:NSManagedObject{
    @NSManaged var name : String
    @NSManaged var score : Int16
}

