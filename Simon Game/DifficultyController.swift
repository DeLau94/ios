//
//  DifficultyController.swift
//  Simon Game
//
//  Created by Laurens Peremans on 20/12/15.
//  Copyright © 2015 Laurens Peremans. All rights reserved.
//

import Foundation
import UIKit
class DifficultyController:UIViewController{
    
    @IBOutlet weak var Difficulty: UISwitch!
    
    override func viewDidLoad() {
        EGC.sharedInstance(self)
        EGC.delegate = self
        let asset = NSDataAsset(name: "cubes")
        let data = asset!.data
        self.view.backgroundColor = UIColor(patternImage: UIImage(data: data)!)

    }
    
    
    @IBAction func gameDifficulty(sender: AnyObject) {
        
        if(Difficulty.on){
            if EGC.isAchievementCompleted(achievementIdentifier: "Veteran") {// gamecenter Source: https://github.com/DaRkD0G/Easy-Game-Center-Swift
                print("achievement is already completed")
            }
            else{
                EGC.showCustomBanner(title: "Veteran", description: "Play a game on hard mode") {
                    EGC.reportAchievement(progress: 100.0, achievementIdentifier: "Veteran")
                    print("posted")
                }
            }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let gameController = segue.destinationViewController as! GameController
        gameController.difficulty = Difficulty.on
    }
 
}