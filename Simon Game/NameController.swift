//
//  NameController.swift
//  Simon Game
//
//  Created by Laurens Peremans on 06/12/15.
//  Copyright © 2015 Laurens Peremans. All rights reserved.
//

import Foundation
import UIKit
import CoreData
class NameController: UIViewController, UITextFieldDelegate{
    @IBOutlet weak var NameTxt: UITextField!
    var score:Int = 0

    
    override func viewDidLoad() {
        super.viewDidLoad()
        let asset = NSDataAsset(name: "cubes")
        let data = asset!.data
        self.view.backgroundColor = UIColor(patternImage: UIImage(data: data)!)

        //keyboard improvements Source:http://stackoverflow.com/questions/25693130/move-textfield-when-keyboard-appears-swift
        self.NameTxt.delegate = self;
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name:UIKeyboardWillShowNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: nil);
        // gamecenter Source: https://github.com/DaRkD0G/Easy-Game-Center-Swift
        EGC.sharedInstance(self)
        EGC.delegate = self
        if(score >= 10){
        if EGC.isAchievementCompleted(achievementIdentifier: "Pro") {
            print("achievement is already completed")
        }
        else{
            EGC.showCustomBanner(title: "Pro", description: "get a score of 10 or more") {
                EGC.reportAchievement(progress: 100.0, achievementIdentifier: "Pro")
                print("posted")
            }
        }
        }
}
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    func keyboardWillShow(sender: NSNotification) {
        self.view.frame.origin.y -= 50
    }
    
    func keyboardWillHide(sender: NSNotification) {
        self.view.frame.origin.y += 50
    }
    
    @IBAction func Go(sender: AnyObject) {
        let app:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        let context:NSManagedObjectContext = app.managedObjectContext
        
        var user = NSEntityDescription.insertNewObjectForEntityForName("User", inManagedObjectContext: context) as NSManagedObject
        user.setValue(NameTxt.text, forKey: "name")
        user.setValue(score, forKey: "score")
        do {
            try context.save()
        } catch {
            print("Something went wrong!")
        }
    }
}