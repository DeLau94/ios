//
//  GameController.swift
//  Simon Game
//
//  Created by Laurens /Users/laurensperemans/Desktop/Simon Game/Simon Game.xcodeprojPeremans on 02/12/15.
//  Copyright © 2015 Laurens Peremans. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class GameController: UIViewController {
    @IBOutlet weak var YellowBtn: UIButton!
    @IBOutlet weak var GreenBtn: UIButton!
    @IBOutlet weak var BlueBtn: UIButton!
    @IBOutlet weak var RedBtn: UIButton!
    @IBOutlet weak var turnLbl: UILabel!
    @IBOutlet weak var scoreLbl: UILabel!
    var difficulty:Bool = false

    var turn = TurnModel()
    var model = ColorModel()
    var colorArray:[Colour] = []
    var timer = NSTimer()
    var resetTimer = NSTimer()
    var addRandom = true
    var redSound: AVAudioPlayer!
    var yellowSound: AVAudioPlayer!
    var greenSound: AVAudioPlayer!
    var blueSound: AVAudioPlayer!
    var time:Double = 1.5
    override func viewDidLoad() {
        setDifficulty()
        LoadButtons()
        LoadSounds()
        disableButtons()
        timer = NSTimer.scheduledTimerWithTimeInterval(time, target: self, selector: "startGame", userInfo: nil, repeats: true)
        EGC.sharedInstance(self)// gamecenter Source: https://github.com/DaRkD0G/Easy-Game-Center-Swift
        EGC.delegate = self
        if EGC.isAchievementCompleted(achievementIdentifier: "FirstGame") {
            print("achievement is already completed")
        }
        else{
            EGC.showCustomBanner(title: "Newcomer", description: "Start you first game") {
                EGC.reportAchievement(progress: 100.0, achievementIdentifier: "FirstGame")
                print("posted")
            }
        }


    }
    func LoadButtons(){
        YellowBtn.backgroundColor = model.yellow.colour
        BlueBtn.backgroundColor = model.blue.colour
        RedBtn.backgroundColor = model.red.colour
        GreenBtn.backgroundColor = model.green.colour
    }
    
    func startGame(){
        if(addRandom){generateRandomColor()}
        ComputerTurn()
    }
    func resetButtons(){
        resetTimer = NSTimer.scheduledTimerWithTimeInterval(0.6, target: self, selector: "LoadButtons", userInfo:nil, repeats:false)
        
    }
    func generateRandomColor(){
        let i = Int(arc4random_uniform(4))
        let color = model.colors[i]
        colorArray.append(color)
        addRandom=false
    
    }
    func ComputerTurn(){
        disableButtons()
        turnLbl.text = turn.computer
        switch(colorArray[turn.count].name){
            case "Blue":
                BlueBtn.backgroundColor = model.lightBlue.colour
                blueSound.play()
                resetButtons()
            case "Red":
                RedBtn.backgroundColor = model.lightRed.colour
                redSound.play()
                resetButtons()
            case "Yellow":
                YellowBtn.backgroundColor = model.lightYellow.colour
                yellowSound.play()
                resetButtons()
            case "Green":
                GreenBtn.backgroundColor = model.lightGreen.colour
                greenSound.play()
                resetButtons()
        default: break
        }
        if(colorArray.count==turn.count+1)
        {
            timer.invalidate()
            UserTurn()
        }
        else{
        turn.count+=1
        }
        
    }
    func UserTurn(){
        turn.count = 0
        resetTimer = NSTimer.scheduledTimerWithTimeInterval(0.6, target: self, selector: "enableButtons", userInfo:nil, repeats:false)

    }
    func arrayControle(color:Colour){

        print(colorArray[turn.count].name," ",turn.count)
        if(colorArray[turn.count].name == color.name){
            if(colorArray.count==turn.count+1){
                turnCompleted()
            }
            else{
                turn.count+=1
            }
        }
        else{
        gameOver()
        }
            }
    func gameOver(){
        turnLbl.text=turn.gameOver
        resetTimer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: "Name", userInfo:nil, repeats:false)
    }
    func Name(){
        performSegueWithIdentifier("name", sender: self)
    
    }
    func turnCompleted(){
        timer = NSTimer.scheduledTimerWithTimeInterval(time, target: self, selector: "startGame", userInfo: nil, repeats: true)
        turn.count = 0
        addRandom = true
        turn.score+=1
        scoreLbl.text = "Score: "+String(turn.score)
    }
    func enableButtons(){
        YellowBtn.enabled=true
        BlueBtn.enabled=true
        RedBtn.enabled=true
        GreenBtn.enabled=true
        turnLbl.text = turn.user
    }
    func disableButtons(){
        YellowBtn.enabled=false
        BlueBtn.enabled=false
        RedBtn.enabled=false
        GreenBtn.enabled=false
    }
    func LoadSounds(){
        let sound1 = NSDataAsset(name: "simonSound1")
        let sound2 = NSDataAsset(name: "simonSound2")
        let sound3 = NSDataAsset(name: "simonSound3")
        let sound4 = NSDataAsset(name: "simonSound4")
        
        do {
            redSound = try AVAudioPlayer(data: sound1!.data)
            blueSound = try AVAudioPlayer(data: sound2!.data)
            yellowSound = try AVAudioPlayer(data: sound3!.data)
            greenSound = try AVAudioPlayer(data: sound4!.data)
        } catch {
            print("sound not loaded :(")
        }
    }
    func setDifficulty(){
        //hard
        if(difficulty){
            time = 0.75
        }
        //medium
        else{
            time = 1.5
        }
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "name" {
        let nameController = segue.destinationViewController as! NameController
        nameController.score = turn.score
        }
    }
    
    @IBAction func Menu(sender: AnyObject) {
        performSegueWithIdentifier("home", sender: self)
    }
    @IBAction func Yellow(sender: AnyObject) {
        YellowBtn.backgroundColor = model.lightYellow.colour
        resetButtons()
        yellowSound.play()
        arrayControle(model.yellow)
        
    }

    @IBAction func Green(sender: AnyObject) {
        GreenBtn.backgroundColor = model.lightGreen.colour
        greenSound.play()
        arrayControle(model.green)
    }

    @IBAction func Blue(sender: AnyObject) {
        BlueBtn.backgroundColor = model.lightBlue.colour
        resetButtons()
        blueSound.play()
        arrayControle(model.blue)
    }

    @IBAction func Red(sender: AnyObject) {
        RedBtn.backgroundColor = model.lightRed.colour
        resetButtons()
        redSound.play()
        arrayControle(model.red)
    }
    


}
