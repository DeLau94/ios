//
//  ViewController.swift
//  Simon Game
//
//  Created by Laurens Peremans on 02/12/15.
//  Copyright © 2015 Laurens Peremans. All rights reserved.
//

import UIKit
import Darwin
import CoreData
class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.hidesBackButton = true  
        let nav = self.navigationController?.navigationBar
        nav?.barStyle = UIBarStyle.Black
        nav?.tintColor = UIColor.yellowColor()
        EGC.sharedInstance(self) // gamecenter Source: https://github.com/DaRkD0G/Easy-Game-Center-Swift
        EGC.delegate = self
        let asset = NSDataAsset(name: "cubes")
        let data = asset!.data
        self.view.backgroundColor = UIColor(patternImage: UIImage(data: data)!)

    }
@IBAction func unwindFromHighscores(segue: UIStoryboardSegue) {
    
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func Exit(sender: AnyObject) {
        exit(0)
    }
}

