//
//  AboutController.swift
//  Simon Game
//
//  Created by Laurens Peremans on 18/12/15.
//  Copyright © 2015 Laurens Peremans. All rights reserved.
//

import Foundation
import UIKit
class AboutController: UIViewController{
    
    @IBOutlet weak var AboutLbl: UILabel!
    @IBOutlet weak var scroll: UIScrollView!


    override func viewDidLoad() {
        let asset = NSDataAsset(name: "cubes")
        let data = asset!.data
        self.view.backgroundColor = UIColor(patternImage: UIImage(data: data)!)

    }

    }