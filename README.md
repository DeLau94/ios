# README #

### What is this repository for? ###

* This app has been created by Laurens Peremans for a school project of HoGent. This game already exists as a board game but is now converted to software.  The app was written in swift and is available to download on bitbucket. It's an exciting color elimination game. The player is responsible for pressing assigned color lenses at certain times to repeat Super Simon's sequence correctly.

** features and explanation **

Gamecenter is implemented so you will need an account to test these features. 

The app has 6 different screens 

on the main menu you can already go to 3 different screens which are:

 * game difficulty
 * about
 * highscores

If you go to game you will have to set the game difficulty first.
After you have set the difficulty you can start the game by pressing play.

**Tip**: Sound is implemented so turn up the volume and focus on the buttons immidiately because one colour will change for a fraction of a second especially if you play on hard mode.

After you have pressed a wrong button the game will take you to the next screen.
Here you have to fill in a name and press go.
The next screen is the high scores screen.
Here you can watch all the scores from previous played games.
Now you can only go back to main menu by pressing on 'Menu' in the upper left corner.
So that was the most important of my application the only screen left that I haven't explained is the about screen.
But that is pretty straight forward.

** frameworks **

 * gamekit --> for gamecenter
 * coredata --> for local data storage
 * AVFoundation --> for sounds

### How do I get set up? ###

* This game has been written in Xcode 7.1 so you will need atleast the same version or newer.(deployed on iOS version 9.1)

### Contribution guidelines ###

* This is the final project if you want to use this to make it bigger and better then contact Laurens Peremans first
 
### Who do I talk to? ###

* Repository is owned by Laurens Peremans so direct your questions to him.